# EEE3088 PiHat Project

This repository has been made as an assignment for the EEE3088F course at UCT. It entails the design of a UPS PiHat. 

The Hat's primary purpose is to act as a back-up power source for a Pi based project, when Pi's primary power source fails, i.e. cuts out suddenly and without warning. To this end, the Hat's function is to fulfil these basic requirements:

1)The Hat must be able to detect when the primary supply to the Pi cuts out.
2)When the primary supply cuts out, the Hat must immediately switch to powering the Pi from its own supply.
3) The Hat must have enough supply to power the Pi until a safe shutdown can occur.
4)The Hat must send a shutdown signal to the Pi when its own supply is critically low.

Contributions:
In order to contribute to this project (not recommended) refer to the main project gitlab repo for basic schematics,simulations and pcb designs as progress occurs: https://gitlab.com/llxmar003/eee3088f-ups-pihat/-/tree/master. 

Note that this repo is under a different license and hence you will be under different terms. 

Lastly one can contact the primary collaborators of this project at the following emails:
NDXBRE004@myuct.ac.za
LLXMAR003@myuct.ac.za
SMXCHR002@myuct.ac.za
